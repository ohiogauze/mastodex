---
Categories: ["pokemon"]
Colour: "Green"
Number: 495
Instance: "vgai.de"
IsPleroma: true
Username: "julian"
TranslationID: "PKMN_495"
Weight: 4950
---