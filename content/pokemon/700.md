---
Categories: ["pokemon"]
Colour: "Pink"
Number: 700
Instance: "elekk.xyz"
Username: "rennykrin"
TranslationID: "PKMN_700"
Weight: 7000
---