---
Categories: ["pokemon"]
Colour: "Green"
Number: 3
Instance: "pleroma.otter.sh"
IsPleroma: true
Username: "dashie"
TranslationID: "PKMN_3"
Weight: 30
---