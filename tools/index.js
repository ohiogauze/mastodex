const fs = require("fs-extra")
const oakdex = require("oakdex-pokedex")
const claimList = require("./claim-list.json")

/**
 * IMAGES STUFF
 */

function copyImageForPokemonID(id, suffix) {
    const oldSuffix = {
        "M": "mega",
        "X": "megax",
        "Y": "megay"
    }[suffix]
    const src = `node_modules/oakdex-pokedex-sprites/icons/${String(id).padStart(3, "0")}-${oldSuffix}.png`
    const dest = `../static/pokemon/${id}-${suffix}.png`
    fs.copyFile(src, dest)
}

/**
 * LANGUAGE STUFF
 */

const i18n = {
    de: {
        PKMN_MNO: "MissingNo."
    },
    en: {
        PKMN_MNO: "MissingNo."
    },
    fr: {
        PKMN_MNO: "MissingNo."
    },
    it: {
        PKMN_MNO: "MissingNo."
    }
}

oakdex.allPokemon().forEach((pkmn) => {
    const i18nKey = `PKMN_${pkmn.national_id}`
    i18n.de[i18nKey] = pkmn.names.de
    i18n.en[i18nKey] = pkmn.names.en
    i18n.fr[i18nKey] = pkmn.names.fr
    i18n.it[i18nKey] = pkmn.names.it

    // copyImageForPokemonID(pkmn.national_id)
})

/**
 * Build all content files.
 * @param {string} path 
 */
async function buildContentFiles(path) {
    Object.keys(i18n.de).forEach((key) => {
        const id = key.slice(5)
        fs.writeFileSync(`${path}/${id}.md`, buildContent(id))
    })
}

/**
 * Build all language files required for internationalisation.
 * @param {string} path 
 */
async function buildLanguageFiles(path) {
    await new Promise(res => fs.remove(path, res))
    await new Promise(res => fs.mkdir(path, res))
    fs.writeFileSync(`${path}/de.toml`, buildLanguageTOML(i18n.de))
    fs.writeFileSync(`${path}/en.toml`, buildLanguageTOML(i18n.en))
    fs.writeFileSync(`${path}/fr.toml`, buildLanguageTOML(i18n.fr))
    fs.writeFileSync(`${path}/it.toml`, buildLanguageTOML(i18n.it))
}

/**
 * Build the language file for a specific TOML
 * @param {Object.<string, string>} arr 
 */
function buildContent (num) {
    const pk = +num && oakdex.findPokemon(+num)
    let id = claimList[num]
    if (id) id = id.replace("https://", "").split("/@")
    return `---
Categories: ["pokemon"]
Colour: "${pk ? pk.color : "Black"}"
Number: ${num}
Instance: "${id ? id[0] : ''}"
Username: "${id ? id[1] : ''}"
TranslationID: "PKMN_${num}"
Weight: ${+num ? num : 10000}0
---`
}

/**
 * Build the language file for a specific TOML
 * @param {Object.<string, string>} arr 
 */
function buildLanguageTOML (arr) {
    return Object.keys(arr).sort((a, b) => {
        return Math.sign(+a.slice(5) - +b.slice(5))
    }).map((id) => {
        return `[${id}]\r\nother = "${arr[id]}"`
    }).join("\r\n")
}

/**
 * INIT STUFF
 */

async function start(path) {
    // await buildContentFiles(`${path}/content/pokemon`)
    // await buildLanguageFiles(`${path}/i18n`)
    // console.log("Built language files")
    const newMegas = {}

    oakdex.allPokemon().forEach((pkmn) => {
        pkmn.mega_evolutions.forEach((mega, index) => {
            let suffix = {
                megax: "X",
                megay: "Y"
            }[mega.image_suffix] || "M"

            const id = `${pkmn.national_id}-${suffix}`
            const num = pkmn.national_id
            fs.writeFileSync(`${path}/content/pokemon/${id}.md`, `---
Categories: ["pokemon"]
Colour: "${pkmn ? pkmn.color : "Black"}"
Number: ${num}
Instance: ""
Username: ""
Mega: "${suffix}"
TranslationID: "PKMN_${num}"
Weight: ${num}${index + 1}
---`)
            copyImageForPokemonID(num, suffix)
        })
    })
}

start("..")